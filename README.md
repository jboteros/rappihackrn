# rappihack

[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](http://standardjs.com/)

- Standard compliant React Native App Utilizing [Ignite](https://github.com/infinitered/ignite)

### Backend:

1.  Url http://54.89.90.153:2580/studio/index.html
2.  Database rappihack
3.  username: rappihack password: rappihack
4.  repo: https://ffalarconj@bitbucket.org/ffalarconj/functions-orientdb-rappi.git

## :arrow_up: How to Setup

**Step 1:** git clone this repo:

**Step 2:** cd to the cloned repo:

**Step 3:** Install the Application with `yarn` or `npm i`

## :arrow_forward: How to Run App

1.  cd to the repo
2.  Run Build for either OS

- for iOS
  - run `react-native run-ios`
- for Android
  - Run Genymotion
  - run `react-native run-android`

## :arrow_forward: Android

APK url: https://www.dropbox.com/s/juo8v7res6et8np/RappiHackOrientPlusRN.apk?dl=0

Keystore
storeFile file("rappihack.Keystore")
storePassword "20rappihack18"
keyAlias "rappihack"
keyPassword "20rappihack18"

## :arrow_forward: Run Device iOS

Open XCODE on the script AppDelegate.m

for debug:

jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];

for Release:

jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
