import React, { Component } from "react";
import { Platform } from "react-native";
import apisauce from "apisauce";

const _headers = [];

if (Platform.OS === "ios") {
  _headers = {
    Accept: "application/json",
    "Content-Type": "application/json"
  };
} else {
  _headers = {
    "Content-Encoding": "gzip",
    "Cache-Control": "no-store",
    Accept: "application/json",
    "Content-Type": "application/json"
  };
  console.log("Platform.OS: " + _headers);
}

const create = (baseURL = "http://54.89.90.153:2580/") => {
  const api = apisauce.create({
    baseURL,

    headers: _headers,

    timeout: 10000
  });

  const getProduct = (product, token) =>
    api.post(
      `command/rappihack/sql`,
      {
        command: `select getProduct({id : \"${product}\"}) as getProduct `
      },

      {
        headers: {
          Authorization: `Basic ${token}`
        }
      }
    );

  const getSwipeCarousel = (size, token) =>
    api.post(
      `command/rappihack/sql`,
      {
        command: `select getSwipeCarousel({size : ${size}}) as getSwipeCarousel `
      },

      {
        headers: {
          Authorization: `Basic ${token}`
        }
      }
    );

  const predictions = (user, limit, token) =>
    api.post(
      `command/rappihack/sql`,
      {
        command: `select predictions(${user}, ${limit}, { swipe : true}) as predictions`
      },

      {
        headers: {
          Authorization: `Basic ${token}`
        }
      }
    );

  const savePreference = (product, user, preference, token) =>
    api.post(
      `command/rappihack/sql`,
      {
        command: `select savePreference({product : ${product},user : ${user},preference : ${preference}}) as savePreference `
      },

      {
        headers: {
          Authorization: `Basic ${token}`
        }
      }
    );

  const hallways = (user, limit, token) =>
    api.post(
      `command/rappihack/sql`,
      {
        command: `select hallways(${user}, ${limit}) as hallways `
      },

      {
        headers: {
          Authorization: `Basic ${token}`
        }
      }
    );

  return {
    getProduct,
    getSwipeCarousel,
    savePreference,
    hallways,
    predictions
  };
};

export default {
  create
};
