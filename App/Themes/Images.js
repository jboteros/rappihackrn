// leave off @2x/@3x
const images = {
  hearthIcon: require("../Images/Icons/hearthIcon.png"),
  cancelIcon: require("../Images/Icons/cancelIcon.png"),
  welcomeImage: require("../Images/Images/welcomeImage.png")
};

export default images;
