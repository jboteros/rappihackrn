import React, { Component } from 'react';
import {
  ScrollView,
  Text,
  Image,
  View,
  Button,
  TouchableOpacity,
  Alert,
  Modal,
  StatusBar
} from 'react-native';
import { Images, Colors, Fonts, Metrics, ApplicationStyles } from '../Themes';

import ProductItemComponent from '../Components/ProductItemComponent';

import Swiper from 'react-native-deck-swiper';
import API from '../Services/Api';
import FJSON from 'format-json';

// Styles
// import styles from "./Styles/HomeAppStyles";

export default class HomeApp extends Component {
  api = {};
  constructor(props) {
    super(props);
    this.state = {
      productCarousel: [],
      hallways: [],
      index: 0,
      modalHallways: false,
      playGame: false,
      welcome: true,
      swipedAllCards: false,
      swipeDirection: '',
      isSwipingBack: true,
      cardIndex: 0,
      token: 'Z3Vlc3Q6Z3Vlc3Q='
    };
    this.api = API.create();
  }

  renderCard = (product, index) => {
    return (
      <View
        style={{
          // flex: 1,
          alignSelf: 'center',
          width: Metrics.screenWidth * 0.7,
          minHeight: Metrics.screenWidth * 0.75,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: Colors.snow,
          flexDirection: 'column',

          borderRadius: 10,
          borderWidth: 1,
          borderColor: '#E8E8E8',
          justifyContent: 'center'
        }}
      >
        <View
          style={{
            flex: 1
          }}
        />
        <Image
          source={{ uri: product.image }}
          style={{
            flex: 6,
            margin: 20,

            width: Metrics.screenWidth * 0.7,

            resizeMode: 'contain'
          }}
        />
        <Text
          style={{
            marginHorizontal: 20,
            marginVertical: 10,
            fontFamily: Fonts.type.bold
          }}
        >
          {product.name}
          {'\n'}
          <Text
            style={{
              fontWeight: '100',
              marginHorizontal: 20,
              marginVertical: 10
            }}
          >
            {product.description}
          </Text>
        </Text>
        <View
          style={{
            flex: 1
          }}
        />
      </View>
    );
  };

  onSwipedAllCards = () => {
    this.setState({
      swipedAllCards: true
    });
  };

  swipeBack = () => {
    if (!this.state.isSwipingBack) {
      this.setIsSwipingBack(true, () => {
        this.swiper.swipeBack(() => {
          this.setIsSwipingBack(false);
        });
      });
    }
  };

  setIsSwipingBack = (isSwipingBack, cb) => {
    this.setState(
      {
        isSwipingBack: isSwipingBack
      },
      cb
    );
  };

  swipeLeft = () => {
    this.swiper.swipeLeft();
  };

  swipeRight = () => {
    this.swiper.swipeRight();
  };

  renderCategorieListComponent() {
    return this.state.hallways[0].hallways.map((endpoint, index) =>
      this.renderCategoriesListComponent(endpoint, index)
    );
  }
  renderCategoriesListComponent(apiEndpoint, index) {
    return (
      <ScrollView
        key={apiEndpoint.category}
        style={{
          width: Metrics.screenWidth * 0.95,
          alignSelf: 'center',
          borderRadius: 10,
          overflow: 'hidden',
          backgroundColor: 'snow',
          marginVertical: 5,
          borderWidth: 1,
          borderColor: '#E5E1E1'
        }}
      >
        <View
          style={{
            width: Metrics.screenWidth * 0.95,
            flex: 0
          }}
        >
          <View
            style={{
              width: Metrics.screenWidth * 0.95,
              height: 50,
              justifyContent: 'center'
            }}
          >
            <Image
              source={{ uri: apiEndpoint.image }}
              style={{
                width: Metrics.screenWidth * 0.95,
                height: 50,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10
              }}
            />
            <View
              style={{
                width: Metrics.screenWidth * 0.95,
                height: 50,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                backgroundColor: Colors.maskView,
                position: 'absolute'
              }}
            />

            <Text
              numberOfLines={1}
              style={{
                fontSize: Fonts.size.regular,
                marginLeft: 20,

                fontWeight: 'bold',
                position: 'absolute',
                color: Colors.snow
              }}
            >
              {apiEndpoint.category}
            </Text>
          </View>

          <ScrollView horizontal={true}>
            {this.renderProductCategorieListComponent(apiEndpoint.products)}
          </ScrollView>
        </View>
      </ScrollView>
    );
  }

  renderProductCategorieListComponent(products) {
    return products.map((endpoint, index) =>
      this.renderCategoriesProductsListComponent(endpoint, index)
    );
  }

  renderCategoriesProductsListComponent(apiEndpoint, index) {
    return (
      <View
        key={apiEndpoint.rid}
        style={{
          flex: 0,
          margin: 10,
          width: 100,
          alignItems: 'center',
          justifyContent: 'center'
          // backgroundColor: "blue"
        }}
      >
        <View style={{ width: 80, height: 80 }}>
          <Image
            source={{ uri: apiEndpoint.image }}
            style={{
              width: 80,
              height: 80,
              resizeMode: 'contain',
              position: 'absolute'
            }}
          />
        </View>

        <Text
          numberOfLines={2}
          style={{
            fontSize: Fonts.size.small,
            fontWeight: 'bold',
            marginVertical: 5,
            textAlign: 'center',
            color: Colors.darkColor
          }}
        >
          {apiEndpoint.name}
        </Text>
        <Text
          numberOfLines={3}
          style={{
            fontSize: Fonts.size.small,
            marginVertical: 5,
            textAlign: 'center',
            color: Colors.grayColor
          }}
        >
          {apiEndpoint.description}
        </Text>
        <Text
          numberOfLines={3}
          style={{
            fontSize: Fonts.size.small,
            marginVertical: 5,
            textAlign: 'center',
            color: Colors.darkColor
          }}
        >
          {this.calculate(apiEndpoint.price)}
        </Text>
      </View>

      // <ScrollView
      //   style={{
      //     width: Metrics.screenWidth,
      //     height: 150,
      //
      //     backgroundColor: "red",
      //     marginVertical: 5
      //   }}
      // />
    );
  }
  render() {
    StatusBar.setBarStyle('dark-content', true);
    return (
      <View
        style={{
          flex: 1,
          width: Metrics.width,
          height: Metrics.height,
          backgroundColor: Colors.snow,
          flexDirection: 'column'
        }}
      >
        <Modal visible={this.state.modalHallways}>
          <View
            style={{
              flex: 1,
              width: Metrics.width,
              height: Metrics.height,
              backgroundColor: Colors.snow,
              marginTop: 20
            }}
          >
            {this.state.hallways.length > 0 ? (
              <ScrollView style={{ flex: 1 }}>
                {this.renderCategorieListComponent()}
              </ScrollView>
            ) : null}

            <View
              style={{
                flex: 0,
                justifyContent: 'center',
                alignItems: 'center'
                // backgroundColor: "red"
              }}
            >
              <TouchableOpacity
                style={{
                  width: Metrics.screenWidth * 0.95,
                  height: 50,
                  marginVertical: 5,
                  backgroundColor: Colors.accentGreen,
                  borderRadius: 10,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
                onPress={() => {
                  this.predictions('#14:0', 5);
                  // this.getSwipeCarousel(5);

                  this.setState(
                    { playGame: false, modalHallways: false },
                    () => {
                      this.setState({ welcome: true });
                    }
                  );
                }}
              >
                <Text
                  style={{
                    color: Colors.snow,
                    fontWeight: 'bold'
                  }}
                >
                  Jugar de nuevo
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal visible={this.state.playGame}>
          <View
            style={{
              flex: 1,
              width: Metrics.width,
              height: Metrics.height,
              backgroundColor: Colors.snow
            }}
          >
            <View style={{ flex: 1 }} />
            <View
              style={{
                flex: 4,
                zIndex: 2000,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Swiper
                ref={swiper => {
                  this.swiper = swiper;
                }}
                onSwiped={this.onSwiped}
                onTapCard={this.swipeRight}
                cards={this.state.productCarousel}
                cardIndex={this.state.index}
                cardVerticalMargin={20}
                renderCard={this.renderCard}
                onSwipedLeft={index => {
                  this.getNewdata(index, -1);
                }}
                onSwipedRight={index => {
                  this.getNewdata(index, 1);
                }}
                onSwipedTop={index => {
                  this.getNewdata(index, 3);
                }}
                onSwipedBottom={index => {
                  this.getNewdata(index, -3);
                }}
                onSwipedAll={this.onSwipedAllCards}
                stackSize={3}
                stackSeparation={15}
                containerStyle={{ backgroundColor: 'transparent' }}
                overlayLabels={{
                  bottom: {
                    title: 'EW...',
                    style: {
                      label: {
                        backgroundColor: Colors.accentRed,
                        color: 'white'
                      },
                      wrapper: {
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'flex-start'
                      }
                    }
                  },
                  left: {
                    title: 'NO',
                    style: {
                      label: {
                        backgroundColor: Colors.accentRed,
                        color: 'white'
                      },
                      wrapper: {
                        flexDirection: 'column',
                        alignItems: 'flex-end',
                        justifyContent: 'flex-start',
                        marginTop: 30,
                        marginLeft: -30
                      }
                    }
                  },
                  right: {
                    title: 'SI',
                    style: {
                      label: {
                        backgroundColor: Colors.accentGreen,
                        color: 'white'
                      },
                      wrapper: {
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        justifyContent: 'flex-start',
                        marginTop: 30,
                        marginLeft: 30
                      }
                    }
                  },
                  top: {
                    title: 'ME ENCANTA',
                    style: {
                      label: {
                        backgroundColor: Colors.accentGreen,
                        color: 'white'
                      },
                      wrapper: {
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center'
                      }
                    }
                  }
                }}
                animateOverlayLabelsOpacity
                animateCardOpacity
              />

              {/* {this.state.productCarousel.length > 0 ? null : (
                <TouchableOpacity
                  onPress={() =>
                this.getSwipeCarousel(5)
                  }
                  style={{
                    // flex: 0,
                    width: 100,
                    height: 50,
                    // backgroundColor: "red",
                    // position: "absolute",
                    justifyContent: "flex-end",
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      color: Colors.accentGreen,
                      textDecorationLine: "underline"
                    }}
                  >
                    Iniciar
                  </Text>
                </TouchableOpacity>
              )} */}
            </View>
            {this.state.productCarousel.length == 0 ? null : (
              <View
                style={{
                  flex: 1,
                  width: Metrics.screenWidth,
                  zIndex: 3000,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center'
                }}
              >
                <View
                  style={{
                    flex: 1,
                    height: 50,
                    backgroundColor: 'transparent'
                  }}
                />
                <TouchableOpacity
                  onPress={this.swipeLeft}
                  style={{
                    flex: 0,
                    width: 80,
                    height: 80,
                    borderRadius: 40,
                    backgroundColor: Colors.accentRed,
                    marginHorizontal: 20,
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  <Image
                    source={Images.cancelIcon}
                    style={{
                      width: 30,
                      height: 30,
                      resizeMode: 'contain'
                    }}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={this.swipeRight}
                  style={{
                    flex: 0,
                    width: 80,
                    height: 80,
                    borderRadius: 40,
                    marginHorizontal: 20,
                    backgroundColor: Colors.accentGreen,
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  <Image
                    source={Images.hearthIcon}
                    style={{
                      width: 40,
                      height: 40,

                      resizeMode: 'contain'
                    }}
                  />
                </TouchableOpacity>
                <View
                  style={{
                    flex: 1,
                    height: 50,
                    backgroundColor: 'transparent'
                  }}
                />
              </View>
            )}

            <View
              style={{
                flex: 1,
                zIndex: 2001,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  // this.setState({ productCarousel: [] });
                  this.hallways('#14:0', 50);
                  this.setState({ welcome: false, playGame: false }, () => {
                    this.setState({ modalHallways: true });
                  });
                }}
              >
                <Text
                  style={{
                    color: Colors.accentGreen,
                    textDecorationLine: 'underline'
                  }}
                >
                  Terminar
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>

        <Modal visible={this.state.welcome}>
          <View
            style={{
              flex: 1,
              width: Metrics.width,
              height: Metrics.height,
              backgroundColor: Colors.snow,
              flexDirection: 'column'
            }}
          >
            <View style={{ flex: 1 }} />
            <View
              style={{
                flex: 6,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Image
                source={Images.welcomeImage}
                style={{
                  margin: 20,
                  resizeMode: 'contain',
                  width: Metrics.screenWidth * 0.7
                }}
              />
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  marginHorizontal: 50,
                  fontFamily: Fonts.type.bold,
                  fontSize: Fonts.size.h5,
                  color: Colors.darkColor,
                  textAlign: 'center'
                }}
              >
                ¿Quieres recibir una sorpresa?
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  marginHorizontal: 50,
                  fontFamily: Fonts.type.base,
                  fontSize: Fonts.size.regular,
                  color: Colors.grayColor,
                  textAlign: 'center'
                }}
              >
                Completa el siguiente juego y recibe un código de cupón para
                redimir en Rappi :)
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <TouchableOpacity
                style={{
                  width: Metrics.screenWidth * 0.8,
                  height: 50,
                  backgroundColor: Colors.accentGreen,
                  borderRadius: 10,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
                onPress={() => {
                  this.predictions('#14:0', 5);
                  // this.getSwipeCarousel(5);

                  this.setState({ welcome: false }, () => {
                    this.setState({ hallways: false, playGame: true });
                  });
                }}
              >
                <Text
                  style={{
                    color: Colors.snow,
                    fontWeight: 'bold'
                  }}
                >
                  ¡Sí, empezar a j ugar!
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <TouchableOpacity
                onPress={
                  () => {
                    this.setState({ welcome: false }, () => {
                      this.hallways('#14:0', 50);
                      this.setState({ modalHallways: true });
                    });
                  }
                  // this.setState({ productCarousel: [] });
                }
              >
                <Text
                  style={{
                    color: Colors.accentGreen,
                    textDecorationLine: 'underline'
                  }}
                >
                  No, Gracias
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }

  getNewdata(index, preference, product) {
    console.tron.log(this.state.productCarousel[index].rid);

    console.tron.log(index);
    this.setState({ index: index });

    this.savePreference(
      this.state.productCarousel[index].rid,
      '#14:0',
      preference
    );
    if (index >= this.state.productCarousel.length - 2) {
      // this.getSwipeCarousel(5);
      this.predictions('#14:0', 5);
    }
  }

  getProduct(product) {
    this.api['getProduct']
      .apply(this, (args = [product, this.state.token]))
      .then(result => {
        this.showResultGetProduct(result);
      });
  }

  showResultGetProduct(response) {
    if (response.status === 200) {
      Alert.alert('OK', FJSON.plain(response.data.result[0].getProduct));
    } else {
      Alert.alert('Bad', response.data.errors[0].content);
    }
  }

  getSwipeCarousel(size) {
    this.api['getSwipeCarousel']
      .apply(this, (args = [size, this.state.token]))
      .then(result => {
        this.showResultGetSwipeCarousel(result);
      });
  }

  showResultGetSwipeCarousel(response) {
    if (response.status === 200) {
      // Alert.alert("OK", FJSON.plain(response.data.result[0].getSwipeCarousel));
      // this.state.productCarousel.push(response.data.result[0].getSwipeCarousel);

      this.setState({
        productCarousel: [
          ...this.state.productCarousel,
          ...response.data.result[0].getSwipeCarousel
        ]
      });

      // this.setState({
      //   productCarousel: response.data.result[0].getSwipeCarousel
      // });
    } else {
      Alert.alert('Bad', response.data.errors[0].content);
    }
  }

  savePreference(product, user, preference) {
    this.api['savePreference']
      .apply(this, (args = [product, user, preference, this.state.token]))
      .then(result => {
        this.showResultSavePreference(result);
      });
  }

  showResultSavePreference(response) {
    if (response.status === 200) {
      // Alert.alert("OK", FJSON.plain(response.data.result[0].savePreference));
    } else {
      // Alert.alert("Bad", response.data.errors[0].content);
    }
  }

  hallways(user, limit) {
    this.api['hallways']
      .apply(this, (args = [user, limit, this.state.token]))
      .then(result => {
        this.showResultHallways(result);
      });
  }

  showResultHallways(response) {
    if (response.status === 200) {
      // Alert.alert("OK", FJSON.plain(response.data.result[0].hallways));

      this.setState(
        {
          hallways: [response.data.result[0]]
        },
        () => {
          console.tron.log(this.state.hallways);
        }
      );
    } else {
      Alert.alert('Bad', response.data.errors[0].content);
    }
  }

  predictions(user, limit) {
    this.api['predictions']
      .apply(this, (args = [user, limit, this.state.token]))
      .then(result => {
        this.showResultPredictionsl(result);
      });
  }

  showResultPredictionsl(response) {
    if (response.status === 200) {
      this.setState({
        productCarousel: [
          ...this.state.productCarousel,
          ...response.data.result[0].predictions
        ]
      });
    } else {
      Alert.alert('Bad', response.data.errors[0].content);
    }
  }

  //Globals
  calculate(amount) {
    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    // 2 = 2 || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) return parseFloat(0).toFixed(2);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(2);

    var amount_parts = amount.split('.'),
      regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
      amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return '$' + amount_parts.join('.');
    // if (num != null) {
    //   var p = num.toFixed(2).split(".");
    //   return (
    //     "$" +
    //     p[0]
    //       .split("")
    //       .reverse()
    //       .reduce(function(acc, num, i, orig) {
    //         return num == "-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    //       }, "")
    //   );
    // }
  }
}
