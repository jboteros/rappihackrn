import { StackNavigator } from "react-navigation";
import HomeApp from "../Containers/HomeApp";

import styles from "./Styles/NavigationStyles";

// Manifest of possible screens
const PrimaryNav = StackNavigator(
  {
    HomeApp: { screen: HomeApp }
  },
  {
    // Default config for all screens
    headerMode: "none",
    initialRouteName: "HomeApp",
    navigationOptions: {
      headerStyle: styles.header
    }
  }
);

export default PrimaryNav;
